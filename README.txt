CONTENTS OF THIS FILE
---------------------
* Introduction
* Requirements
* Installation
* Configuration
* Permissions
* Maintainers


Introduction
------------
The Commerce Cart Stats module tracks the number of times products are
added or removed from a cart and when products' quantities are changed in
the shopping cart itself. It also tracks cart abandonment, the number of
times a product is in a cart that is abandoned, and where in the checkout
process that abandonment occurs.

The stats can be viewed under 'Store > Cart Statistics' or by going to
/admin/commerce/reports/cart.


Requirements
------------
This module requires the following modules:
* Commerce (https://drupal.org/project/commerce)


Installation
------------
Install as you would normally install a contributed drupal module. See:
https://drupal.org/documentation/install/modules-themes/modules-7
for further information.


Configuration
-------------
No configuration necessary.


Permissions
-----------
If someone other than user 1 needs to view this page, set the "Access cart
statistics" permission for that user's role.


MAINTAINERS
-----------
Current maintainers:
* Adam Fuller (dasfuller) - https://drupal.org/user/2731951
